# FREE NOW backend applicant test

## Solution
All the tasks mentioned in README.MD has been handled.

### To check code coverage
`mvn clean package`

Below file shows the code coverage currently it is 92%.

`server-applicant-test-21/target/site/jacoco-ut/index.html`

## About api implemented

To know complete api details please refer to swagger. I have used basic auth. So when prompted please enter 

`username/password as vishwas/vishwas` 

`http://localhost:8080/swagger-ui.html`

Postman collection is also available in project `FreeNow.postman_collection.json`

Below description about the api's are in the order of the flow which I thought while designing.

## CREATE CAR 'POST "/v1/cars"'
Creates cars based on its characteristics and manufacturers. Any number of cars can be created with this api with multiple brands. This also allows same car to be added multiple times. Status of the cars is made as available at this stage.

## CREATE DRIVER 'POST /v1/drivers'
Any number of drivers can be added using this api. No 2 drivers can have same name. Status of the driver is made as offline at this stage.

## FIND DRIVER BY STATUS "GET /v1/drivers?onlineStatus=OFFLINE"
All the drivers can be found with status.

## FIND CARS BY STATUS "GET /v1/cars?carStatus=AVAILABLE"
All the cars available will be returned.

## UPDATE STATUS OF DRIVER TO ONLINE "PUT /v1/drivers/driverid/6/status/ONLINE"
Only online drivers are allowed to book cars.

## SELECT CAR WITH IDs "POST /v1/select/driverid/6/carid/3"
We use the ids of cars returned above from Find cars and Find drivers to select car for the driver.

## GET SEARCH PARAMS "GET /v1/search/params"
Returns the list of search params avaliable 

`'{
    "carparams": [
        "license_plate",
        "seat_count",
        "convertible",
        "rating",
        "engine_type"
    ],
    "driverparams": [
        "username",
        "online_status"
    ]
}'`

## SEARCH FOR DRIVER "POST /v1/search"
Body
``{
    "carparams": {
    	"engine_type":"ELECTRIC"
    },
    "driverparams": {
    	"username":"vishwas"
    }
  }``
  
  You can use any of the above search params to search for driver. Output is a list of unique drivers.
  
  ## DESELECT DRIVER "POST /v1/deselect/driverid/6"
  Unselects cars for the specified driver.
  
  There are few more endpoints(CRUD for car). PLease refer to swagger for details.
  
  ## LIMITATIONS
  * Used Basic auth should change this to oath.
  * Should have used cache to store car details.
  * Currently search is using native query. Need to see if this can be achived by jpa for dynamic where clause. But for first version I am happy with this.
  

