package com.freenow.datatransferobject;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class SearchDTO
{
    Map<String, String> driverparams = new HashMap<>();
    Map<String, String> carparams = new HashMap<>();
}
