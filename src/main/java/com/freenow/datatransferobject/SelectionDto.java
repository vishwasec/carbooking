package com.freenow.datatransferobject;

import java.util.Map;

import lombok.Data;

@Data
public class SelectionDto
{
    private Map<String, String> driverparams;
    private Map<String, String> carparams;
}
