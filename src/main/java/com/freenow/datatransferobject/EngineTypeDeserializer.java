package com.freenow.datatransferobject;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.freenow.domainobject.EngineType;

public class EngineTypeDeserializer extends JsonDeserializer<EngineType>
{
    @Override
    public EngineType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException
    {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);

        if (node == null)
        {
            return null;
        }

        String text = node.textValue();

        if (text == null)
        {
            return null;
        }

        return EngineType.valueOf(text);
    }

}
