package com.freenow.datatransferobject;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.freenow.domainobject.EngineType;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
public class CarDTO
{

    @ApiModelProperty(hidden = true)
    private long id;

    @NotNull(message = "carname can not be null!")
    private String carname;

    private CarInfoDto carInfo;

    private CarManufacturers carManufacturers;

    @Data
    public static class CarInfoDto
    {

        private String licensePlate;
        private int seatCount;
        private boolean convertible;
        private int rating;
        private EngineType engineType;

    }

    @Data
    public static class CarManufacturers
    {
        private String name;
    }
}
