package com.freenow.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;

@Configuration
@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "search")
@Getter
@Setter
public class CarApplicationProps
{
    private List<String> driverparams = new ArrayList<>();
    private List<String> carparams = new ArrayList<>();
    private String username;
    private String password;
}
