package com.freenow.service.selection;

import static com.freenow.domainvalue.CarStatus.AVAILABLE;
import static com.freenow.domainvalue.OnlineStatus.ONLINE;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.freenow.domainobject.CarDO;
import com.freenow.domainobject.DriverDO;
import com.freenow.domainvalue.CarStatus;
import com.freenow.exception.CarAlreadyInUseException;
import com.freenow.exception.ConstraintsViolationException;
import com.freenow.exception.EntityNotFoundException;
import com.freenow.service.car.CarService;
import com.freenow.service.driver.DriverService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class CarSelectionServiceImpl implements CarSelectionService
{

    private DriverService driverService;

    private CarService carService;


    @Override
    @Transactional
    public void selectCar(long driverId, long carId)
        throws EntityNotFoundException,
        ConstraintsViolationException, CarAlreadyInUseException
    {
        CarDO carDO = carService.find(carId);
        DriverDO driverDO = driverService.find(driverId);

        if (driverDO.getOnlineStatus() != ONLINE)
        {
            throw new ConstraintsViolationException("User is not online");
        }
        else if (carDO.getCarStatus() != AVAILABLE)
        {
            throw new CarAlreadyInUseException("Car already booked.");
        }

        carDO.setCarStatus(CarStatus.BOOKED);
        driverDO.setCarDO(carDO);
        driverService.createOrUpdate(driverDO);
    }


    @Override
    public void deselectCar(long driverId) throws EntityNotFoundException, ConstraintsViolationException
    {
        DriverDO driverDO = driverService.find(driverId);

        if (!ObjectUtils.isEmpty(driverDO))
        {

            CarDO carDO = driverDO.getCarDO();

            if (!ObjectUtils.isEmpty(carDO))
            {
                carDO.setCarStatus(CarStatus.AVAILABLE);
                carService.createOrUpdate(carDO);
            }
        }

        driverDO.setCarDO(null);
        driverService.createOrUpdate(driverDO);
    }
}
