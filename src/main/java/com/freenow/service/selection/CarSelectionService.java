package com.freenow.service.selection;

import com.freenow.exception.CarAlreadyInUseException;
import com.freenow.exception.ConstraintsViolationException;
import com.freenow.exception.EntityNotFoundException;

public interface CarSelectionService
{

    void selectCar(long driverId, long carId) throws EntityNotFoundException, ConstraintsViolationException, CarAlreadyInUseException;


    void deselectCar(long driverId) throws EntityNotFoundException, ConstraintsViolationException;

}
