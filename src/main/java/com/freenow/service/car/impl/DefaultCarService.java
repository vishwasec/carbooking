package com.freenow.service.car.impl;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freenow.dataaccessobject.CarRepository;
import com.freenow.domainobject.CarDO;
import com.freenow.domainobject.CarInfo;
import com.freenow.domainobject.CarManufacturers;
import com.freenow.domainvalue.CarStatus;
import com.freenow.exception.ConstraintsViolationException;
import com.freenow.exception.EntityNotFoundException;
import com.freenow.service.car.CarInfoService;
import com.freenow.service.car.CarManufacturersService;
import com.freenow.service.car.CarService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
@AllArgsConstructor
@Slf4j
public class DefaultCarService implements CarService
{

    private CarRepository carRepository;

    private CarManufacturersService carManufacturerService;

    private CarInfoService carInfoService;


    @Override
    @Transactional(readOnly = true)
    public CarDO find(Long carId) throws EntityNotFoundException
    {
        return carRepository
            .findById(carId)
            .orElseThrow(() -> new EntityNotFoundException("Could not find car entity with id: " + carId));
    }


    @Override
    @Transactional
    public CarDO createOrUpdate(CarDO carDO) throws ConstraintsViolationException
    {
        CarDO car;
        try
        {
            carDO.setCarStatus(CarStatus.AVAILABLE);

            CarManufacturers carManufacturers =
                carManufacturerService
                    .getOrSave(carDO.getCarManufacturers());

            CarInfo carInfo = carInfoService.save(carDO.getCarInfo());

            carDO.setCarInfo(carInfo);
            carDO.setCarManufacturers(carManufacturers);
            car = carRepository.save(carDO);
        }
        catch (DataIntegrityViolationException e)
        {
            log.warn("ConstraintsViolationException while creating a car: {}", carDO, e);
            throw new ConstraintsViolationException(e.getMessage());
        }

        return car;
    }


    @Override
    @Transactional
    public void delete(Long carId)
    {
        carRepository.deleteById(carId);
    }


    @Override
    @Transactional(readOnly = true)
    public List<CarDO> find(CarStatus carStatus)
    {
        return carRepository.findCarDOByCarStatus(carStatus);
    }

}
