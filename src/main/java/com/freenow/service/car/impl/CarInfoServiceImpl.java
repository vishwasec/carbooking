package com.freenow.service.car.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.freenow.dataaccessobject.CarInfoRepository;
import com.freenow.domainobject.CarInfo;
import com.freenow.service.car.CarInfoService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
@AllArgsConstructor
@Slf4j
public class CarInfoServiceImpl implements CarInfoService
{

    private CarInfoRepository carInfoRepository;


    @Override
    @Transactional
    public CarInfo save(CarInfo carInfo)
    {
        return carInfoRepository.save(carInfo);
    }

}
