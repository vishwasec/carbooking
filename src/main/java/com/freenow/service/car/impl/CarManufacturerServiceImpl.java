package com.freenow.service.car.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.freenow.dataaccessobject.CarManufacturersRepository;
import com.freenow.domainobject.CarManufacturers;
import com.freenow.service.car.CarManufacturersService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
@AllArgsConstructor
@Slf4j
public class CarManufacturerServiceImpl implements CarManufacturersService
{

    private CarManufacturersRepository carManufacturersRepository;


    @Override
    @Transactional
    public CarManufacturers getOrSave(CarManufacturers carManufacturers)
    {
        CarManufacturers carmanf = carManufacturersRepository.findByName(carManufacturers.getName());

        return ObjectUtils.isEmpty(carmanf) ? carManufacturersRepository.save(carManufacturers) : carmanf;

    }

}
