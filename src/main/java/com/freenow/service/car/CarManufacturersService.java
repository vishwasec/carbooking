package com.freenow.service.car;

import com.freenow.domainobject.CarManufacturers;

public interface CarManufacturersService
{
    CarManufacturers getOrSave(CarManufacturers carManufacturers);
}
