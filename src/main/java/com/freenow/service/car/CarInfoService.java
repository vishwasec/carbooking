package com.freenow.service.car;

import com.freenow.domainobject.CarInfo;

public interface CarInfoService
{

    CarInfo save(CarInfo carInfo);
}
