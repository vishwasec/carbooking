package com.freenow.service.search;

import java.util.Collection;
import java.util.Map;

import com.freenow.domainobject.DriverDO;

public interface SearchService
{

    Collection<DriverDO> searchDriver(Map<String, String> driverparams, Map<String, String> carparams);

}
