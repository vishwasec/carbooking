package com.freenow.service.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.freenow.config.CarApplicationProps;
import com.freenow.dataaccessobject.custom.SearchDriverRepository;
import com.freenow.domainobject.DriverDO;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
@AllArgsConstructor
@Slf4j
public class SearchServiceImpl implements SearchService
{

    private final CarApplicationProps carApplicationProps;
    private final SearchDriverRepository searchDriverRepository;


    @Override
    public Collection<DriverDO> searchDriver(
        Map<String, String> driverparams,
        Map<String, String> carparams)
    {
        List<DriverDO> driverDOList = new ArrayList<>();

        if (driverparams != null && CollectionUtils.containsAny(carApplicationProps.getDriverparams(), driverparams.keySet()))
        {
            driverDOList.addAll(findByDriver(driverparams));
        }
        if (carparams != null && CollectionUtils.containsAny(carApplicationProps.getCarparams(), carparams.keySet()))
        {
            driverDOList.addAll(findBycar(carparams));
        }
        return driverDOList
            .stream()
            .<Map<Long, DriverDO>>collect(HashMap::new, (m, e) -> m.put(e.getId(), e), Map::putAll)
            .values();
    }


    private List<DriverDO> findByDriver(Map<String, String> driverparams)
    {
        return searchDriverRepository.findbydriverparams(driverparams);
    }


    private List<DriverDO> findBycar(Map<String, String> carparams)
    {
        return searchDriverRepository.findbydriverByCar(carparams);
    }

}
