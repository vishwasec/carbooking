package com.freenow.controller.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.freenow.datatransferobject.CarDTO;
import com.freenow.domainobject.CarDO;
import com.freenow.domainobject.CarInfo;
import com.freenow.domainobject.CarManufacturers;

public class CarMapper
{

    public static CarDO makeCarDO(CarDTO carDTO)
    {
        CarDO carDO = new CarDO();
        CarInfo carInfo = new CarInfo();
        CarManufacturers carManufacturers = new CarManufacturers();

        carDO.setCarInfo(carInfo);
        carDO.setCarManufacturers(carManufacturers);

        BeanUtils.copyProperties(carDTO, carDO);
        BeanUtils.copyProperties(carDTO.getCarInfo(), carInfo);
        BeanUtils.copyProperties(carDTO.getCarManufacturers(), carManufacturers);

        return carDO;
    }


    public static CarDTO makeCarDto(CarDO carDO)
    {
        CarDTO.CarInfoDto carInfoDto = new CarDTO.CarInfoDto();
        CarDTO.CarManufacturers manufacturers = new CarDTO.CarManufacturers();

        BeanUtils.copyProperties(carDO.getCarInfo(), carInfoDto);
        BeanUtils.copyProperties(carDO.getCarManufacturers(), manufacturers);

        return CarDTO
            .builder()
            .id(carDO.getId())
            .carname(carDO.getCarname())
            .carInfo(carInfoDto)
            .carManufacturers(manufacturers)
            .build();

    }


    public static List<CarDTO> makeCarDto(List<CarDO> carDOS)
    {
        return carDOS.stream().map(CarMapper::makeCarDto).collect(Collectors.toList());
    }
}
