package com.freenow.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.freenow.exception.CarAlreadyInUseException;
import com.freenow.exception.ConstraintsViolationException;
import com.freenow.exception.EntityNotFoundException;
import com.freenow.service.selection.CarSelectionService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("v1")
@Slf4j
@AllArgsConstructor
public class SelectionController
{
    private final CarSelectionService carSelectionService;


    @PostMapping("select/driverid/{driverid}/carid/{carid}")
    public ResponseEntity<String> selectCar(@PathVariable long driverid, @PathVariable long carid)
        throws ConstraintsViolationException, EntityNotFoundException, CarAlreadyInUseException
    {

        carSelectionService.selectCar(driverid, carid);
        return ResponseEntity
            .status(HttpStatus.OK)
            .body("Selection successful. ");
    }


    @PostMapping("deselect/driverid/{driverid}")
    public ResponseEntity<String> deselectCar(@PathVariable long driverid)
        throws ConstraintsViolationException, EntityNotFoundException
    {
        carSelectionService.deselectCar(driverid);
        return ResponseEntity
            .status(HttpStatus.OK)
            .body("De Selection successful. ");
    }

}
