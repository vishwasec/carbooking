package com.freenow.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.freenow.config.CarApplicationProps;
import com.freenow.controller.mapper.DriverMapper;
import com.freenow.datatransferobject.DriverDTO;
import com.freenow.datatransferobject.SearchDTO;
import com.freenow.service.search.SearchService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("v1/search")
@Slf4j
@AllArgsConstructor
public class SearchController
{
    private static final String CARPARAMS = "carparams";
    private static final String DRIVERPARAMS = "driverparams";
    private final SearchService searchService;
    private final CarApplicationProps carApplicationProps;


    @PostMapping()
    public List<DriverDTO> search(@RequestBody SearchDTO searchDTO)
    {

        return searchService
            .searchDriver(
                searchDTO.getDriverparams(),
                searchDTO.getCarparams())
            .stream().map(DriverMapper::makeDriverDTO)
            .collect(Collectors.toList());
    }


    @GetMapping("params")
    public Map<String, List<String>> getSearchParams()
    {
        Map<String, List<String>> searchParams = new HashMap<>();
        searchParams.put(CARPARAMS, carApplicationProps.getCarparams());
        searchParams.put(DRIVERPARAMS, carApplicationProps.getDriverparams());
        return searchParams;
    }
}
