package com.freenow.domainobject;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.freenow.datatransferobject.EngineTypeDeserializer;

@JsonDeserialize(using = EngineTypeDeserializer.class)
public enum EngineType
{
    ELECTRIC, GAS
}
