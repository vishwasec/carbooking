package com.freenow.domainobject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@ToString(includeFieldNames = true)
@Entity
@Table(name = "car_info")
@Data
public class CarInfo
{
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "license_plate")
    private String licensePlate;
    @Column(name = "seat_count")
    private int seatCount;
    @Column(name = "convertible")
    private boolean convertible;
    @Column(name = "rating")
    private int rating;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "engine_type")
    private EngineType engineType;

}
