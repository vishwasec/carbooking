package com.freenow.domainobject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Table(name = "car_manufacturers")
@Data
public class CarManufacturers
{
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @NotNull(message = "manufacturers can not be null!")
    private String name;

}
