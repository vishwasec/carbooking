package com.freenow.dataaccessobject;

import org.springframework.data.repository.CrudRepository;

import com.freenow.domainobject.CarManufacturers;

public interface CarManufacturersRepository extends CrudRepository<CarManufacturers, Long>
{
    CarManufacturers findByName(String name);
}
