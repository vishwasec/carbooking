package com.freenow.dataaccessobject.custom;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.context.annotation.Configuration;

import com.freenow.domainobject.DriverDO;

@Configuration
public class UserRepoCustomImpl implements SearchDriverRepository
{
    private static final String DRIVER_SELECT_SQL = "SELECT * FROM driver %s";
    private static final String DRIVER_JOIN_SQL =
        "SELECT d.* FROM driver d inner join (SELECT ci.*,c.id as "
            +
            "cid FROM car_info ci inner join car c on c.car_info_id = ci.id %s) t on d.car_id=t.cid";
    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<DriverDO> findbydriverparams(Map<String, String> driverparams)
    {
        StringBuilder whereclause = new StringBuilder("where ");
        for (String key : driverparams.keySet())
        {
            whereclause.append(key).append(" = '").append(driverparams.get(key)).append("'");
        }
        return entityManager
            .createNativeQuery(String.format(DRIVER_SELECT_SQL, whereclause), DriverDO.class)
            .getResultList();
    }


    @Override
    public List<DriverDO> findbydriverByCar(Map<String, String> carparams)
    {
        StringBuilder whereclause = new StringBuilder("where ");
        for (String key : carparams.keySet())
        {
            if (key.equals("seat_count") || key.equals("rating"))
            {
                whereclause.append("ci.").append(key).append(" = ").append(carparams.get(key));
            }
            else
            {
                whereclause.append("ci.").append(key).append(" = '").append(carparams.get(key)).append("'");
            }
        }
        return entityManager
            .createNativeQuery(String.format(DRIVER_JOIN_SQL, whereclause), DriverDO.class)
            .getResultList();
    }
}
