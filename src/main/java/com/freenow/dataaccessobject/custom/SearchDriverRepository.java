package com.freenow.dataaccessobject.custom;

import java.util.List;
import java.util.Map;

import com.freenow.domainobject.DriverDO;

public interface SearchDriverRepository
{
    List findbydriverparams(Map<String, String> driverparams);


    List<DriverDO> findbydriverByCar(Map<String, String> carparams);
}
