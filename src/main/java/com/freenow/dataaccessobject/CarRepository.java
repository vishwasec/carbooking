package com.freenow.dataaccessobject;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.freenow.domainobject.CarDO;
import com.freenow.domainvalue.CarStatus;

public interface CarRepository extends CrudRepository<CarDO, Long>
{

    List<CarDO> findCarDOByCarStatus(CarStatus carStatus);
}
