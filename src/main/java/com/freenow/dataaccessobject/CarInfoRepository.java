package com.freenow.dataaccessobject;

import org.springframework.data.repository.CrudRepository;

import com.freenow.domainobject.CarInfo;

public interface CarInfoRepository extends CrudRepository<CarInfo, Long>
{}
