package com.freenow.controller.mapper;

import static com.freenow.controller.mapper.CarMapper.makeCarDto;
import static com.freenow.service.utils.MockUtils.MARUTHI;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.freenow.datatransferobject.CarDTO;
import com.freenow.domainobject.CarDO;
import com.freenow.service.utils.MockUtils;

public class CarMapperTest
{

    @Test
    public void makeDriverDO()
    {
        //run
        CarDTO carDTO = makeCarDto(MockUtils.mockCar());

        //verify
        assertNotNull(carDTO);
        assertEquals(MARUTHI, carDTO.getCarname());
    }


    @Test
    public void makeDriverDTO()
    {
        //when
        CarDTO carDTO = makeCarDto(MockUtils.mockCar());

        //run
        CarDO carDO = CarMapper.makeCarDO(carDTO);

        //verify
        assertNotNull(carDO);
        assertEquals(MARUTHI, carDO.getCarname());
    }


    @Test
    public void makeDriverDTOList()
    {
        //run
        List<CarDTO> carDTOS = Collections.singletonList(makeCarDto(MockUtils.mockCar()));

        //verify
        assertNotNull(carDTOS);
        assertEquals(1, carDTOS.size());
    }
}
