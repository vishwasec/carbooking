package com.freenow.controller.mapper;

import static com.freenow.service.utils.MockUtils.mockDriver1;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.freenow.datatransferobject.DriverDTO;
import com.freenow.domainobject.DriverDO;

public class DriverMapperTest
{

    @Test
    public void makeDriverDTO()
    {
        //run
        DriverDTO driverDTO = DriverMapper.makeDriverDTO(mockDriver1());

        //verify
        assertNotNull(driverDTO);
        assertEquals(driverDTO.getUsername(), "vish1");
        assertEquals(mockDriver1().getCoordinate(), driverDTO.getCoordinate());

    }


    @Test
    public void makeDriverDO()
    {
        //when
        DriverDTO driverDTO = DriverMapper.makeDriverDTO(mockDriver1());

        //run
        DriverDO driverDO = DriverMapper.makeDriverDO(driverDTO);

        //verify
        assertNotNull(driverDO);
        assertEquals("vish1", driverDO.getUsername());

    }


    @Test
    public void makeDriverDTOList()
    {

        //run
        List<DriverDTO> driverDO = DriverMapper.makeDriverDTOList(Collections.singleton(mockDriver1()));

        //verify
        assertNotNull(driverDO);
        assertEquals(1, driverDO.size());

    }
}
