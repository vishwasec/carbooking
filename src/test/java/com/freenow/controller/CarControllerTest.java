package com.freenow.controller;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.freenow.FreeNowServerApplicantTestApplication;
import com.freenow.domainvalue.CarStatus;
import com.freenow.service.car.CarService;
import com.freenow.service.utils.MockUtils;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(FreeNowServerApplicantTestApplication.class)

public class CarControllerTest
{
    private MockMvc mvc;
    @Mock
    private CarService carService;
    @InjectMocks
    private CarController carController;


    @Before
    public void setUp()
    {
        mvc =
            MockMvcBuilders
                .standaloneSetup(carController)
                .build();
    }


    @Test
    public void createCar() throws Exception
    {
        //when
        Mockito.when(carService.createOrUpdate(Mockito.any())).thenReturn(MockUtils.mockCar());

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .post("/v1/cars").content(
                        "{\n"
                            +
                            "  \"carInfo\": {\n" +
                            "    \"convertible\": true,\n" +
                            "    \"engineType\": \"ELECTRIC\",\n" +
                            "    \"licensePlate\": \"string\",\n" +
                            "    \"rating\": 0,\n" +
                            "    \"seatCount\": 0\n" +
                            "  },\n" +
                            "  \"carManufacturers\": {\n" +
                            "    \"name\": \"maruthi\"\n" +
                            "  },\n" +
                            "  \"carname\": \"zen\"\n" +
                            "}")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isCreated());

    }


    @Test
    public void getCar() throws Exception
    {
        //when
        Mockito.when(carService.find(1l)).thenReturn(MockUtils.mockCar());

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .get("/v1/cars/carid/1")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }


    @Test
    public void getCarByStatus() throws Exception
    {
        //when
        Mockito.when(carService.find(CarStatus.AVAILABLE)).thenReturn(Collections.singletonList(MockUtils.mockCar()));

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .get("/v1/cars?carStatus=AVAILABLE")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }


    @Test
    public void deleteCar() throws Exception
    {
        mvc
            .perform(
                MockMvcRequestBuilders
                    .delete("/v1/cars/carid/1")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
