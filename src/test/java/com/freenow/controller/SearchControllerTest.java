package com.freenow.controller;

import static com.freenow.service.utils.MockUtils.mockCarAppProps;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.freenow.FreeNowServerApplicantTestApplication;
import com.freenow.config.CarApplicationProps;
import com.freenow.service.search.SearchService;
import com.freenow.service.utils.MockUtils;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(FreeNowServerApplicantTestApplication.class)

public class SearchControllerTest
{
    private static final String CARPARAMS = "carparams";
    private static final String DRIVERPARAMS = "driverparams";
    @Mock
    private SearchService searchService;
    @Mock
    private CarApplicationProps carApplicationProps;

    @InjectMocks
    private SearchController searchController;

    private MockMvc mvc;


    @Before
    public void setUp()
    {
        carApplicationProps = mockCarAppProps();
        ReflectionTestUtils.setField(searchController, "carApplicationProps", carApplicationProps);

        mvc =
            MockMvcBuilders
                .standaloneSetup(searchController)
                .build();
    }


    @Test
    public void search() throws Exception
    {
        //when
        Mockito.when(searchService.searchDriver(Mockito.any(), Mockito.any())).thenReturn(Collections.singleton(MockUtils.mockDriver1()));

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .post("/v1/search").content(
                        "{\n"
                            +
                            "  \"carparams\": {\n" +
                            "  \t\"engine_type\":\"ELECTRIC\"\n" +
                            "  },\n" +
                            "  \"driverparams\": {\n" +
                            "  }\n" +
                            "}")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }


    @Test
    public void getSearchParams() throws Exception
    {

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .get("/v1/search/params")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
