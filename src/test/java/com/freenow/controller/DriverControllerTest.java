package com.freenow.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.freenow.FreeNowServerApplicantTestApplication;
import com.freenow.service.driver.DriverService;
import com.freenow.service.utils.MockUtils;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(FreeNowServerApplicantTestApplication.class)

public class DriverControllerTest
{
    private MockMvc mvc;
    @Mock
    private DriverService driverService;
    @InjectMocks
    private DriverController driverController;


    @Before
    public void setUp()
    {
        mvc =
            MockMvcBuilders
                .standaloneSetup(driverController)
                .build();
    }


    @Test
    public void getDriver() throws Exception
    {
        //when
        Mockito.when(driverService.find(1l)).thenReturn(MockUtils.mockDriver1());

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .get("/v1/drivers/1")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }


    @Test
    public void createDriver() throws Exception
    {
        //when
        Mockito.when(driverService.createOrUpdate(Mockito.any())).thenReturn(MockUtils.mockDriver1());

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .post("/v1/drivers").content(
                        "{\n"
                            +
                            "  \"coordinate\": {\n" +
                            "    \"latitude\": 0,\n" +
                            "    \"longitude\": 0\n" +
                            "  },\n" +
                            "  \"password\": \"gopala\",\n" +
                            "  \"username\": \"vish\"\n" +
                            "}")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isCreated());

    }


    @Test
    public void deleteDriver() throws Exception
    {
        mvc
            .perform(
                MockMvcRequestBuilders
                    .delete("/v1/drivers/1")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }


    @Test
    public void updateLocation()
    {}


    @Test
    public void updateStatus() throws Exception
    {
        //when
        Mockito.when(driverService.updateDriverStatus(23l, "ONLINE")).thenReturn(MockUtils.mockDriver1());

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .put("/v1/drivers/driverid/23/status/ONLINE")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }


    @Test
    public void findDrivers() throws Exception
    {

        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .get("/v1/drivers?onlineStatus=OFFLINE")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
