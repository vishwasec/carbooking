package com.freenow.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.freenow.FreeNowServerApplicantTestApplication;
import com.freenow.service.selection.CarSelectionService;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(FreeNowServerApplicantTestApplication.class)

public class SelectionControllerTest
{
    @Mock
    private CarSelectionService carSelectionService;

    @InjectMocks
    private SelectionController selectionController;
    private MockMvc mvc;


    @Before
    public void setUp()
    {
        mvc =
            MockMvcBuilders
                .standaloneSetup(selectionController)
                .build();
    }


    @Test
    public void selectCar() throws Exception
    {
        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .post("/v1/select/driverid/1/carid/2")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }


    @Test
    public void deselectCar() throws Exception
    {
        //run
        mvc
            .perform(
                MockMvcRequestBuilders
                    .post("/v1/deselect/driverid/1")
                    .contentType("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
