package com.freenow.service.utils;

import static com.freenow.domainvalue.CarStatus.AVAILABLE;

import java.util.Arrays;
import java.util.List;

import com.freenow.config.CarApplicationProps;
import com.freenow.domainobject.CarDO;
import com.freenow.domainobject.CarInfo;
import com.freenow.domainobject.CarManufacturers;
import com.freenow.domainobject.DriverDO;
import com.freenow.domainobject.EngineType;
import com.freenow.domainvalue.GeoCoordinate;
import com.freenow.domainvalue.OnlineStatus;

public class MockUtils
{
    public static final String MARUTHI = "Maruthi";
    public static final List<String> CARPARAMS = Arrays.asList("license_plate", "seat_count", "convertible", "rating", "engine_type");
    public static final List<String> DRIVERPARAMS = Arrays.asList("username", "online_status");


    public static CarInfo mockCarInfo()
    {
        CarInfo carInfo = new CarInfo();
        carInfo.setConvertible(true);
        carInfo.setEngineType(EngineType.ELECTRIC);
        carInfo.setSeatCount(4);
        return carInfo;
    }


    public static CarManufacturers mockCarManufacturers()
    {
        CarManufacturers carManufacturers = new CarManufacturers();
        carManufacturers.setName(MARUTHI);
        return carManufacturers;
    }


    public static CarDO mockCar()
    {
        CarDO carDO = new CarDO();
        carDO.setId(1l);
        carDO.setCarStatus(AVAILABLE);
        carDO.setCarInfo(mockCarInfo());
        carDO.setCarname(MARUTHI);
        carDO.setCarManufacturers(mockCarManufacturers());
        return carDO;
    }


    public static DriverDO mockDriver1()
    {
        DriverDO driverDO = new DriverDO("vish1", "vish");
        driverDO.setId(1l);
        GeoCoordinate geoCoordinate = new GeoCoordinate(1.0, 1.1);
        driverDO.setCoordinate(geoCoordinate);
        driverDO.setOnlineStatus(OnlineStatus.ONLINE);
        driverDO.setCarDO(mockCar());
        return driverDO;
    }


    public static DriverDO mockDriver2()
    {
        DriverDO driverDO = new DriverDO("vish2", "vish");
        driverDO.setId(2l);
        driverDO.setOnlineStatus(OnlineStatus.OFFLINE);

        return driverDO;
    }


    public static CarApplicationProps mockCarAppProps()
    {
        CarApplicationProps carApplicationProps = new CarApplicationProps();
        carApplicationProps.setUsername("vishwas");
        carApplicationProps.setPassword("vishwas");
        carApplicationProps.setCarparams(CARPARAMS);
        carApplicationProps.setDriverparams(DRIVERPARAMS);
        return carApplicationProps;
    }

}
