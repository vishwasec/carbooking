package com.freenow.service.driver;

import static com.freenow.service.utils.MockUtils.mockDriver1;
import static java.util.Optional.ofNullable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import com.freenow.dataaccessobject.DriverRepository;
import com.freenow.domainvalue.OnlineStatus;
import com.freenow.exception.ConstraintsViolationException;
import com.freenow.exception.EntityNotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class DefaultDriverServiceTest
{
    @Mock
    private DriverRepository driverRepository;

    @InjectMocks
    private DefaultDriverService defaultDriverService;


    @Test
    public void find() throws EntityNotFoundException
    {
        //when
        when(driverRepository.findById(anyLong())).thenReturn(ofNullable(mockDriver1()));

        //run
        defaultDriverService.find(anyLong());

        //verify
        verify(driverRepository, times(1)).findById(anyLong());

    }


    @Test(expected = EntityNotFoundException.class)
    public void findException() throws EntityNotFoundException
    {

        //run
        defaultDriverService.find(anyLong());

        //verify
        verify(driverRepository, times(1)).findById(anyLong());

    }


    @Test
    public void createOrUpdate() throws ConstraintsViolationException
    {
        //when
        when(driverRepository.save(any())).thenReturn(mockDriver1());

        //run
        defaultDriverService.createOrUpdate(mockDriver1());

        //verify
        verify(driverRepository, times(1)).save(any());

    }


    @Test(expected = ConstraintsViolationException.class)
    public void createOrUpdateException() throws ConstraintsViolationException
    {
        //when
        when(driverRepository.save(any())).thenThrow(new DataIntegrityViolationException("Failed"));

        //run
        defaultDriverService.createOrUpdate(mockDriver1());

        //verify
        verify(driverRepository, times(1)).save(any());

    }


    @Test
    public void delete() throws EntityNotFoundException
    {
        //when
        when(driverRepository.findById(anyLong())).thenReturn(ofNullable(mockDriver1()));

        //run
        defaultDriverService.delete(anyLong());

        //verify
        verify(driverRepository, times(1)).findById(anyLong());

    }


    @Test
    public void updateLocation() throws EntityNotFoundException
    {
        //when
        when(driverRepository.findById(anyLong())).thenReturn(ofNullable(mockDriver1()));

        //run
        defaultDriverService.updateLocation(anyLong(), 1.0, 1.1);

        //verify
        verify(driverRepository, times(1)).findById(anyLong());

    }


    @Test
    public void findByStatus()
    {
        //when
        when(driverRepository.findByOnlineStatus(OnlineStatus.ONLINE)).thenReturn(Collections.singletonList(mockDriver1()));

        //run
        defaultDriverService.find(OnlineStatus.ONLINE);

        //verify
        verify(driverRepository, times(1)).findByOnlineStatus(any());

    }


    @Test
    public void updateDriverStatus() throws ConstraintsViolationException, EntityNotFoundException
    {
        //when
        when(driverRepository.findById(anyLong())).thenReturn(ofNullable(mockDriver1()));

        when(driverRepository.save(any())).thenReturn(mockDriver1());

        //run
        defaultDriverService.updateDriverStatus(anyLong(), OnlineStatus.ONLINE.name());

        //verify
        verify(driverRepository, times(1)).save(any());
        verify(driverRepository, times(1)).findById(any());

    }
}
