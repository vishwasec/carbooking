package com.freenow.service.search;

import static com.freenow.service.utils.MockUtils.mockCarAppProps;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.freenow.config.CarApplicationProps;
import com.freenow.dataaccessobject.custom.SearchDriverRepository;
import com.freenow.domainobject.DriverDO;
import com.freenow.service.utils.MockUtils;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceImplTest
{

    CarApplicationProps carApplicationProps;
    @InjectMocks
    private SearchServiceImpl searchService;
    @Mock
    private SearchDriverRepository searchDriverRepository;


    @Before
    public void init()
    {
        carApplicationProps = mockCarAppProps();
        ReflectionTestUtils.setField(searchService, "carApplicationProps", carApplicationProps);
    }


    @Test
    public void searchDriver()
    {
        //when
        Map<String, String> carParams = new HashMap<>();
        Map<String, String> driverParams = new HashMap<>();
        carParams.put("license_plate", "1234");
        driverParams.put("username", "vish1");

        Mockito.when(searchDriverRepository.findbydriverparams(driverParams)).thenReturn(Arrays.asList(MockUtils.mockDriver1()));
        Mockito.when(searchDriverRepository.findbydriverByCar(carParams)).thenReturn(Arrays.asList(MockUtils.mockDriver2()));

        //run
        Collection<DriverDO> driverDOS = searchService.searchDriver(driverParams, carParams);

        //verify
        verify(searchDriverRepository, times(1)).findbydriverparams(any());
        verify(searchDriverRepository, times(1)).findbydriverByCar(any());
        assertEquals(2, driverDOS.size());

    }
}
