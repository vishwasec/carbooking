package com.freenow.service.car.impl;

import static com.freenow.service.utils.MockUtils.MARUTHI;
import static com.freenow.service.utils.MockUtils.mockCarManufacturers;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.freenow.dataaccessobject.CarManufacturersRepository;
import com.freenow.domainobject.CarManufacturers;

@RunWith(MockitoJUnitRunner.class)
public class CarManufacturerServiceImplTest
{

    @InjectMocks
    private CarManufacturerServiceImpl carManufacturerService;

    @Mock
    private CarManufacturersRepository carManufacturersRepository;


    @Test
    public void getOrSave()
    {
        //when
        CarManufacturers mockCarManufacturers = mockCarManufacturers();
        Mockito.when(carManufacturerService.getOrSave(mockCarManufacturers)).thenReturn(mockCarManufacturers);

        //run
        CarManufacturers carManufacturers = carManufacturerService.getOrSave(mockCarManufacturers);

        //validate
        Mockito.verify(carManufacturersRepository, Mockito.times(1)).save(mockCarManufacturers);
        assertEquals(MARUTHI, carManufacturers.getName());
    }

}
