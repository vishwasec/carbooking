package com.freenow.service.car.impl;

import static com.freenow.service.utils.MockUtils.mockCarInfo;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.freenow.dataaccessobject.CarInfoRepository;
import com.freenow.domainobject.CarInfo;

@RunWith(MockitoJUnitRunner.class)
public class CarInfoServiceImplTest
{

    @Mock
    private CarInfoRepository carInfoRepository;

    @InjectMocks
    private CarInfoServiceImpl carInfoService;


    @Test
    public void save()
    {
        //when
        CarInfo mockCarInfo = mockCarInfo();
        when(carInfoRepository.save(any())).thenReturn(mockCarInfo);

        //run
        CarInfo carInfo = carInfoService.save(mockCarInfo);

        //validation
        verify(carInfoRepository, times(1)).save(mockCarInfo);
        assertTrue(carInfo.isConvertible());
    }

}
