package com.freenow.service.car.impl;

import static com.freenow.service.utils.MockUtils.mockCar;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import com.freenow.dataaccessobject.CarRepository;
import com.freenow.domainobject.CarDO;
import com.freenow.domainobject.CarInfo;
import com.freenow.domainobject.CarManufacturers;
import com.freenow.domainvalue.CarStatus;
import com.freenow.exception.ConstraintsViolationException;
import com.freenow.exception.EntityNotFoundException;
import com.freenow.service.car.CarInfoService;
import com.freenow.service.car.CarManufacturersService;
import com.freenow.service.utils.MockUtils;

@RunWith(MockitoJUnitRunner.class)
public class DefaultCarServiceTest
{

    @Mock
    private CarRepository carRepository;

    @Mock
    private CarManufacturersService carManufacturerService;

    @Mock
    private CarInfoService carInfoService;

    @InjectMocks
    private DefaultCarService defaultCarService;


    @Test
    public void find() throws EntityNotFoundException
    {
        //when
        Optional<CarDO> moockCar = Optional.of(mockCar());
        when(carRepository.findById(Mockito.anyLong())).thenReturn(moockCar);

        //run
        CarDO carDO = defaultCarService.find(1l);

        //validate
        verify(carRepository, times(1)).findById(1l);
        assertNotNull(carDO);
        assertEquals(CarStatus.AVAILABLE, carDO.getCarStatus());

    }


    @Test(expected = EntityNotFoundException.class)
    public void findException() throws EntityNotFoundException
    {
        //when
        Optional<CarDO> moockCar = Optional.of(mockCar());
        when(carRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());

        //run
        CarDO carDO = defaultCarService.find(1l);

        //validate
        verify(carRepository, times(1)).findById(1l);
        assertNotNull(carDO);
        assertEquals(CarStatus.AVAILABLE, carDO.getCarStatus());

    }


    @Test
    public void createOrUpdate() throws ConstraintsViolationException
    {
        //when
        CarManufacturers mockCarManufacturers = MockUtils.mockCarManufacturers();
        CarInfo mockCarInfo = MockUtils.mockCarInfo();
        CarDO mockcarDO = MockUtils.mockCar();
        when(
            carManufacturerService
                .getOrSave(any())).thenReturn(mockCarManufacturers);
        when(
            carInfoService
                .save(any())).thenReturn(mockCarInfo);
        when(carRepository.save(any())).thenReturn(mockcarDO);

        //run
        CarDO carDO = defaultCarService.createOrUpdate(mockcarDO);

        //validate
        verify(carRepository, times(1)).save(mockcarDO);
        verify(carManufacturerService, times(1)).getOrSave(mockCarManufacturers);
        verify(carInfoService, times(1)).save(mockCarInfo);
        assertNotNull(carDO);

    }


    @Test(expected = ConstraintsViolationException.class)
    public void createOrUpdateNegative() throws ConstraintsViolationException
    {
        //when
        CarManufacturers mockCarManufacturers = MockUtils.mockCarManufacturers();
        CarInfo mockCarInfo = MockUtils.mockCarInfo();
        CarDO mockcarDO = MockUtils.mockCar();
        when(
            carManufacturerService
                .getOrSave(any())).thenReturn(mockCarManufacturers);
        when(
            carInfoService
                .save(any())).thenReturn(mockCarInfo);
        when(carRepository.save(any())).thenThrow(DataIntegrityViolationException.class);

        //run
        CarDO carDO = defaultCarService.createOrUpdate(mockcarDO);

        //validate
        verify(carRepository, times(1)).save(mockcarDO);
        verify(carManufacturerService, times(1)).getOrSave(mockCarManufacturers);
        verify(carInfoService, times(1)).save(mockCarInfo);
        assertNotNull(carDO);

    }


    @Test
    public void delete() throws EntityNotFoundException
    {
        //run
        defaultCarService.delete(1l);

        //verify
        verify(carRepository, times(1)).deleteById(1l);

    }


    @Test
    public void findByCarstatus()
    {
        //when
        CarDO mockcarDO = MockUtils.mockCar();
        when(carRepository.findCarDOByCarStatus(CarStatus.AVAILABLE)).thenReturn(Collections.singletonList(mockcarDO));

        //run
        List<CarDO> carDOList = defaultCarService.find(CarStatus.AVAILABLE);

        //verify
        verify(carRepository, times(1)).findCarDOByCarStatus(CarStatus.AVAILABLE);
        assertEquals(Collections.singletonList(mockcarDO), carDOList);

    }
}
