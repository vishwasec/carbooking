package com.freenow.service.selection;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.freenow.exception.CarAlreadyInUseException;
import com.freenow.exception.ConstraintsViolationException;
import com.freenow.exception.EntityNotFoundException;
import com.freenow.service.car.CarService;
import com.freenow.service.driver.DriverService;
import com.freenow.service.utils.MockUtils;

@RunWith(MockitoJUnitRunner.class)
public class CarSelectionServiceImplTest
{

    @Mock
    private DriverService driverService;

    @Mock
    private CarService carService;

    @InjectMocks
    private CarSelectionServiceImpl carSelectionService;


    @Test
    public void selectCar() throws EntityNotFoundException, ConstraintsViolationException, CarAlreadyInUseException
    {
        //when
        Mockito.when(carService.find(ArgumentMatchers.anyLong())).thenReturn(MockUtils.mockCar());
        Mockito.when(driverService.find(ArgumentMatchers.anyLong())).thenReturn(MockUtils.mockDriver1());

        //run
        carSelectionService.selectCar(1l, 1l);

        //verify
        verify(carService, times(1)).find(anyLong());
        verify(driverService, times(1)).find(anyLong());

    }


    @Test(expected = ConstraintsViolationException.class)
    public void selectCarEException() throws EntityNotFoundException, ConstraintsViolationException, CarAlreadyInUseException
    {
        //when
        Mockito.when(carService.find(ArgumentMatchers.anyLong())).thenReturn(MockUtils.mockCar());
        Mockito.when(driverService.find(ArgumentMatchers.anyLong())).thenReturn(MockUtils.mockDriver2());

        //run
        carSelectionService.selectCar(1l, 1l);

        //verify
        verify(carService, times(1)).find(anyLong());
        verify(driverService, times(1)).find(anyLong());

    }


    @Test
    public void deselectCar() throws EntityNotFoundException, ConstraintsViolationException
    {
        //when
        Mockito.when(carService.createOrUpdate(any())).thenReturn(MockUtils.mockCar());
        Mockito.when(driverService.find(ArgumentMatchers.anyLong())).thenReturn(MockUtils.mockDriver1());

        //run
        carSelectionService.deselectCar(1l);

        //verify
        verify(carService, times(1)).createOrUpdate(any());
        verify(driverService, times(1)).find(anyLong());

    }
}
